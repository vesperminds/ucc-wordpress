/*jslint node: true */

'use strict';

var THEME = 'wp-content/themes/ucc-v1';
var ASSETS = THEME + '/assets';
var SCRIPTS_BASE = ASSETS + '/scripts';
var CSS_DIST = THEME + '/dist/css';
var JS_TPL_DIST = THEME + '/dist/js/templates';
var JS_DIST = THEME + '/dist/js/scripts';

var PATHS = {

  build: {

    stylesheets: [
      ASSETS + '/stylesheets/**/*.scss',
      '!' + ASSETS + '/stylesheets/**/_*.scss'
    ],
    templates: [
      ASSETS + '/templates/**/*.html',
    ],
    scripts: [
      'app.ts'
    ]

  },

  watch: {

    stylesheets: [
      ASSETS + '/stylesheets/**/*.scss'
    ],
    scripts: [
      ASSETS + '/scripts/**/*.ts'
    ],
    templates: [
      ASSETS + '/templates/**/*.html'
    ]

  },

  dist: {
    css: CSS_DIST,
    jsTpl: JS_TPL_DIST,
    js: JS_DIST
  }

};

var BROWSERLIST = '> 1%, last 3 versions, IE 8, IE 9, Firefox ESR';

var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var postcss = require('gulp-postcss');
var del = require('del');
var autoprefixer = require('autoprefixer');
var pixrem = require('pixrem');
var livereload = require('gulp-livereload');
var cache = require('gulp-cached');
var source = require('vinyl-source-stream');
var browserify = require('browserify');
var tsify = require('tsify');
var streamify = require('gulp-streamify');
var uglify = require('gulp-uglify');
var templateCache = require('gulp-angular-templatecache');

gulp.task('clean:css', function () {
  return del([CSS_DIST]);
});

gulp.task('clean:jsTpl', function () {
  return del([JS_TPL_DIST]);
});

gulp.task('clean:js', function () {
  return del([JS_DIST]);
});

gulp.task('clean', ['clean:css', 'clean:jsTpl', 'clean:js']);

gulp.task('sass', ['clean:css'], function () {

  var onError = function (err) {

    var msg = err.message;
    msg = msg.replace(ASSETS + '/stylesheets/', '');

    notify.onError({
      title: 'SASS',
      subtitle: 'Build error',
      message: msg,
      sound: false
    })(err);

    this.emit('end');

  };

  var processors = [
    pixrem(undefined, {
      browsers: BROWSERLIST
    }),
    autoprefixer({
      browsers: BROWSERLIST
    })
  ];

  return gulp.src(PATHS.build.stylesheets)
    .pipe(sourcemaps.init())
    .pipe(plumber({ errorHandler: onError }))
    .pipe(sass({
      outputStyle: 'expanded'
    }).on('error', sass.logError))
    .pipe(postcss(processors))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(PATHS.dist.css))
    .pipe(livereload());

});

gulp.task('templates', ['clean:jsTpl'], function () {

  return gulp.src(PATHS.build.templates)
    .pipe(templateCache())
    .pipe(gulp.dest(PATHS.dist.jsTpl))
    .pipe(livereload());

});

gulp.task('typescript', ['clean:js'], function () {

  var bundler = browserify({
    basedir: SCRIPTS_BASE,
    debug: true
  });

  for (var i in PATHS.build.scripts) {
    bundler.add(PATHS.build.scripts[i]);
  }

  bundler.plugin(tsify);

  return bundler.bundle()
    .on('error', function (e) {
      console.log(e.message);

      this.emit('end');
    })
    .pipe(source('app.js'))
    .pipe(gulp.dest(PATHS.dist.js))
    .pipe(livereload());

});

gulp.task('sass:dist', ['clean:css'], function () {

  var onError = function (err) {

    var msg = err.message;
    msg = msg.replace(ASSETS + '/stylesheets/', '');

    notify.onError({
      title: 'SASS',
      subtitle: 'Build error',
      message: msg,
      sound: false
    })(err);

    this.emit('end');

  };

  var processors = [
    pixrem(undefined, {
      browsers: BROWSERLIST
    }),
    autoprefixer({
      browsers: BROWSERLIST
    })
  ];

  return gulp.src(PATHS.build.stylesheets)
    .pipe(plumber({ errorHandler: onError }))
    .pipe(sass({
      outputStyle: 'compressed'
    }).on('error', sass.logError))
    .pipe(postcss(processors))
    .pipe(gulp.dest(PATHS.dist.css));

});

gulp.task('templates:dist', ['clean:jsTpl'], function () {

  return gulp.src(PATHS.build.templates)
    .pipe(templateCache())
    .pipe(uglify())
    .pipe(gulp.dest(PATHS.dist.jsTpl));

});

gulp.task('typescript:dist', ['clean:js'], function () {

  var bundler = browserify({
    basedir: SCRIPTS_BASE,
    debug: false
  });

  for (var i in PATHS.build.scripts) {
    bundler.add(PATHS.build.scripts[i]);
  }

  bundler.plugin(tsify);

  return bundler.bundle()
    .on('error', function (e) {
      console.log(e.message);

      this.emit('end');
    })
    .pipe(source('app.js'))
    .pipe(streamify(uglify()))
    .pipe(gulp.dest(PATHS.dist.js));

});

gulp.task('watch', function () {

  // Listening for LR
  livereload.listen();

  // Watchers
  gulp.watch(PATHS.watch.stylesheets, ['sass']);
  gulp.watch(PATHS.watch.templates, ['templates']);
  gulp.watch(PATHS.watch.scripts, ['typescript']);

});

gulp.task('build', ['sass:dist', 'templates:dist', 'typescript:dist']);
gulp.task('default', ['watch', 'sass', 'templates', 'typescript']);
