<?php defined('ABSPATH') or die;

add_action('init', function() {

    /* Type */

    register_post_type('career', [
        'labels' => [
            'name' => 'Carreras',
            'singular_name' => 'Carreras',
            'menu_name' => 'Carreras',
            'name_admin_bar' => 'Carrera',
            'add_new' => 'Agregar nueva',
            'add_new_item' => 'Agregar nueva carrera',
            'new_item' => 'Nueva carrera',
            'edit_item' => 'Editar carrera',
            'view_item' => 'Ver carrera',
            'all_items' => 'Todas las carreras',
            'search_items' => 'Buscar carreras',
            'parent_item_color' => 'Carrera padre:',
            'not_found' => 'No se encontraron carreras.',
            'not_found_in_trash' => 'No se encontraron carreras en la papelera.',
        ],
        'public' => true,
        'menu_position' => 20,
        'menu_icon' => 'dashicons-welcome-learn-more',
        'capability_type' => 'page',
        'supports' => [
            'title',
            'editor',
            'thumbnail',
            'page-attributes'
        ],
        'rewrite' => [
            'slug' => 'carrera'
        ]
    ]);

    /* Type Meta */

    // VP_MetaBox::registerForCustomPostType('room', [
    //     'id' => 'room-information',
    //     'title' => 'Room Information',
    //     'context' => 'normal',
    //     'priority' => 'default',

    //     'group-options' => [ ],

    //     'fields' => [

    //         'booking-id' => [
    //             'label' => 'Booking ID',
    //             'type' => 'text',
    //             'maxlength' => 60,
    //         ]

    //     ]
    // ]);

    /* Type taxonomy */

    register_taxonomy('career_types', 'career', [
        'labels' => [
            'name' => 'Tipos de carrera',
            'singular_name' => 'Tipo de carrera',
            'all_items' => 'Todos los tipos de carrera',
            'edit_item' => 'Editar tipo de carrera',
            'view_item' => 'Ver tipo de carrera',
            'update_item' => 'Actualizar tipo de carrera',
            'add_new_item' => 'Agregar nuevo tipo de carrera',
            'new_item_name' => 'Nombre del nuevo tipo de carrera',
            'parent_item' => 'Tipo de carrera padre',
            'parent_item_colon' => 'Tipo de carrera padre:',
            'search_items' => 'Buscar tipos de carrera',
            'popular_items' => 'Tipos de carrera populares',
            'separate_items_with_commas' => 'Separar tipos de carrera con comas',
            'add_or_remove_items' => 'Agregar o quitar tipos de carrera',
            'choose_from_most_used' => 'Elegir los más usados tipos de carrera',
            'not_found' => 'No se encontraron tipos de carrera.',
            'menu_name' => 'Tipos de carrera',
        ],
        'public' => true,
        'show_tagcloud' => false,
        'show_in_quick_edit' => true,
        'show_admin_column' => true,
        'hierarchical' => true
    ]);
    register_taxonomy_for_object_type('career_types', 'career');

});

/* Type helper */

function get_career_types(array $opts = []) {
    $opts = array_merge([
        'orderby' => 'name',
        'hide_empty' => true,
    ], $opts);

    $opts['taxonomy'] = 'career_types';

    $terms = get_terms($opts);

    return array_map(function($value) {
        return (array) $value;
    }, $terms);
}

function get_all_career_types(array $opts = []) {
    return get_career_types(array_merge([
        'hide_empty' => false
    ], $opts));
}

function get_all_careers(array $opts = [], array $meta = []) {

    $opts = array_merge([
        'type' => 'career',
        'order' => 'ASC',
        'orderby' => 'menu_order',
        'thumbsize' => 'vesper-post-thumbnail-cover',
    ], $opts);

    if (isset($meta['term'])) {
        $opts['taxonomy'] = 'career_types';
        $opts['taxonomyid'] = $meta['term'];
    }

    $all = vp_get_all($opts);

    if (function_exists('get_images_src')) {
        array_walk($all, function(&$career) {
            $career['images'] = get_images_src('vesper-gallery-image', false, $career['id']);
        });
    }

    return $all;
}
