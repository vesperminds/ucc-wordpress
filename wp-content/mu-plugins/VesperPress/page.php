<?php

function vp_inject_pages_from_options($name) {
  $ids = get_option($name, []);

  if (!is_array($ids)) {
    $ids = [ $ids ];
  }

  $query = new WP_Query([
    'post_type' => 'page',
    'post__in' => $ids,
    'orderby' => 'none',
  ]);

  return $query;
}

/**
* Injects a single page
* @param  string  $slug
* @return boolean
*/
function vp_inject_single_page($slug) {
  $query = new WP_Query([
    'pagename' => $slug
  ]);

  $have = $query->have_posts();

  if ($have) {
    $query->the_post();
  } else {
    wp_reset_postdata();
  }

  return $have;
}

/**
* Finishes query injection
* @return void
*/
function vp_inject_end() {
  wp_reset_postdata();
}
