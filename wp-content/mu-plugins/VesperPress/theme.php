<?php defined('ABSPATH') or die;

/* Blog URL helper */

function vp_url($path = '/') {

	return get_site_url(null, $path);

}

function vp_is_active($path) {
	global $post;

	if ($path == '@home-page' && (is_home() || is_front_page())) {
		return true;
	}

	if (isset($post) && isset($post->post_name)) {
		if ('/' . $post->post_name == substr($path, strrpos($path, '/'))) {
			return true;
		}
	}

	return false;
}

/* Theme files helper functions */

function vpth_path($path = '/') {

	return get_stylesheet_directory_uri() . $path;

}

function vpth_version($path = '/') {

	$file = get_stylesheet_directory() . $path;

	if (is_readable($file) && is_file($file)) {

		$ver = (string) filemtime($file);

		if (!$ver) {
			$ver = date('YmdH');
		}

	}
	else {
		$ver = date('YmdH');
	}

	return $ver;

}
