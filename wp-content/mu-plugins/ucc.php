<?php defined('ABSPATH') or die;
/**
 * Plugin Name: UCC
 * Description: Non-theme settings for Universidad Castro Carazo
 * Author: Vesper Minds
 * Author URI: http://vesperminds.com/
 * Network: true
 */

require_once __DIR__ . '/ucc/careers.php';

/** changing default wordpres email settings */

add_filter('wp_mail_from', function($old = '') {
	return 'no-reply@umca.net';
});

add_filter('wp_mail_from_name', function($old = '') {
	return 'Universidad Castro Carazo';
});

/**
 * Wordpress footer
 */
add_filter('admin_footer_text', function($text) {
	return $text . ' <i>Potenciado por <a href="http://vesperminds.com/" target="_blank">Vesper Minds</a><i>';
});
