declare var $, Swiper;

export class UCCSimpleHelper {
  mobilePaneBodyClass: string = 'mobile-pane--menu-opened';

  private body: HTMLElement;
  
  private careerSlider;

  constructor() {
    this.body = document.body;
  }

  bootstrap() {
    this.setupMobilePaneToggle();
    this.setupCareerSlider();
  }

  setupMobilePaneToggle() {
    let $body = $(this.body);

    $('#mobile-pane-toggle-button').on('click', e => {
      e.preventDefault();

      $body.toggleClass(this.mobilePaneBodyClass);
    });

    $('[mobile-pane-ignore-click]').on('click', e => e.stopPropagation());

    $body.on('click', e => {
      if (!$body.hasClass(this.mobilePaneBodyClass)) {
        return;
      }

      $body.removeClass(this.mobilePaneBodyClass);
    });
  }

  setupCareerSlider() {
    this.careerSlider = new Swiper('.careers .swiper-container', {
      slidesPerView: 'auto',
      nextButton: '.careers__slider-button-next',
      prevButton: '.careers__slider-button-prev',
    });
  }
}
