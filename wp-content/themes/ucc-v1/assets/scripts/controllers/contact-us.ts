declare var angular;

export class ContactUsController {
  model = {
    first_name: '',
    last_name: '',
    phone: '',
    email: '',
    subject: '',
    message: '',
  }
  endpoint: string = '';
  baseUrl: string = '';

  render: boolean = false;
  inProgress: boolean = false;
  success: boolean = false;

  error: string = '';

  static $inject = ['$timeout', '$http'];
  constructor(
    private timeout,
    private http
  ) { }

  $onInit() {
    this.timeout(() => {
      this.render = true;
    }, 1000);
  }

  send(formEl) {
    if (formEl.$invalid) {
      return;
    }

    this.inProgress = true;
    this.http.post(this.endpoint, this.model)
      .then(response => {
        if (!response) {
          throw new Error('Internal server error');
        } else if (response.error) {
          throw new Error(response.error);
        }

        this.success = true;
        this.reset();
      })
      .catch(e => {
        if (!e) {
          this.error = 'Unknown error';
        } else if (typeof e === 'string') {
          this.error = e;
        } else if (e instanceof Error) {
          this.error = e.message;
        } else if (e.statusText) {
          this.error = `(${e.status}) ${e.statusText}`;
        } else {
          this.error = '?';
        }
      })
      .finally(() => {
        this.inProgress = false;
      });
  }

  reset() {
    this.model.first_name = '';
    this.model.last_name = '';
    this.model.phone = '';
    this.model.email = '';
    this.model.subject = '';
    this.model.message = '';
  }
}
