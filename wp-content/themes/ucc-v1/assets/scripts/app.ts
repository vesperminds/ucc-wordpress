declare var angular, $;

import { UCCSimpleHelper } from './helpers/ucc-simple';

import { ContactUsComponent } from './components/contact-us';

let templates = angular.module('templates', []),
  app = angular.module('ngAppUCC', ['templates']);

app.component('contactUs', ContactUsComponent);

if (!(<any>window)._DEBUG) {
  app.config(['$compileProvider', function ($compileProvider) {
    $compileProvider.debugInfoEnabled(false);
  }]);
}

// Helpers

$(() => (new UCCSimpleHelper).bootstrap());
