<?php
    $careers = get_all_careers();
?>
<section class="container careers">
    <div class="row">
        <div class="gr-12">
            <h2>Carreras</h2>
        </div>
    </div>

    <div class="row row-full">
        <div class="swiper-container careers__slider">
            <div class="swiper-wrapper">
                <?php foreach ($careers as $career): ?>
                <div class="swiper-slide">
                    <a href="<?= $career['permalink'] ?>"
                        class="careers__slide"
                        style="background-image: url('<?= $career['thumb'] ?>')"
                    >
                        <span class="careers__slide-title"><?= $career['title'] ?></span>
                    </a>
                </div>
                <?php endforeach; ?>
            </div>

            <div class="careers__slider-button-prev">
                <i class="fa fa-fw fa-chevron-circle-left"></i>
            </div>
            <div class="careers__slider-button-next">
                <i class="fa fa-fw fa-chevron-circle-right"></i>
            </div>
        </div>
    </div>
</section>
