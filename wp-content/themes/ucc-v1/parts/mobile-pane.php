<div class="mobile-pane">
    <a href="#" class="mobile-pane__menu-toggle" id="mobile-pane-toggle-button" mobile-pane-ignore-click>
        <i class="fa fa-fw fa-bars"></i>
    </a>
    <div class="mobile-pane__logo"></div>
</div>
