<div class="nav-wrapper">
    <nav class="container nav">
        <div class="row">
            <div class="gr-adapt">
                <span class="nav__logo">
                    <a href="#" class="nav__logo-image"></a>
                </span>
            </div>
            <div class="gr-grow">
                <ul class="nav__items">
                    <li class="nav__item">
                        <a href="<?= vp_url('/') ?>">Inicio</a>
                    </li>
                    <li class="nav__item">
                        <a href="<?= vp_url('/nosotros') ?>">Nosotros</a>
                    </li>
                    <li class="nav__item">
                        <a href="<?= vp_url('/admision') ?>">Admisión</a>
                    </li>
                    <li class="nav__item">
                        <a href="<?= vp_url('/carreras') ?>">Carreras</a>
                    </li>
                    <li class="nav__item">
                        <a href="<?= vp_url('/noticias') ?>">Noticias</a>
                    </li>
                    <li class="nav__item">
                        <a href="<?= vp_url('/contacto') ?>">Contáctenos</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>

<nav class="mobile-nav" mobile-pane-ignore-click>
    <ul class="mobile-nav__items">
        <li class="mobile-nav__item">
            <a href="<?= vp_url('/') ?>">Inicio</a>
        </li>
        <li class="mobile-nav__item">
            <a href="<?= vp_url('/nosotros') ?>">Nosotros</a>
        </li>
        <li class="mobile-nav__item">
            <a href="<?= vp_url('/admision') ?>">Admisión</a>
        </li>
        <li class="mobile-nav__item">
            <a href="<?= vp_url('/carreras') ?>">Carreras</a>
        </li>
        <li class="mobile-nav__item">
            <a href="<?= vp_url('/noticias') ?>">Noticias</a>
        </li>
        <li class="mobile-nav__item">
            <a href="<?= vp_url('/contacto') ?>">Contáctenos</a>
        </li>
    </ul>
</nav>
