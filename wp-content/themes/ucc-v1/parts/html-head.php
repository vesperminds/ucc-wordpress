<!DOCTYPE html>
<html lang="es" ng-app="ngAppUCC" ng-strict-di>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <title><?= th_title() ?></title>
    <link rel="stylesheet" href="https://use.fontawesome.com/88265fc2f5.css">
    <?php wp_head(); ?>
</head>
<body>
