<div class="footer-wrapper">
    <footer class="container footer">
        <div class="row">
            <nav class="gr-2 gr-12@mobile footer__inner-links">
                <ul>
                    <li class="footer__inner-link">
                        <a href="<?= vp_url('/nosotros') ?>">¿Porqué UCC?</a>
                    </li>
                    <li class="footer__inner-link">
                        <a href="<?= vp_url('/admision') ?>">Admisión</a>
                    </li>
                    <li class="footer__inner-link">
                        <a href="<?= vp_url('/carreras') ?>">Carreras</a>
                    </li>
                    <li class="footer__inner-link">
                        <a href="<?= vp_url('/contacto') ?>">Contacto</a>
                    </li>
                </ul>
            </nav>

            <div class="gr-8 gr-12@mobile footer__people">
                <div class="row">
                    <div class="gr-12">
                        <p>
                            Si desea contactar con nuestros departamentos, haga click en el nombre de la persona encargada
                            para enviar un correo electrónico.
                        </p>
                    </div>

                    <div class="gr-2 gr-12@mobile">
                        <h4>Registro</h4>
                        <ul>
                            <li><a href="#">Marcela Morales</a></li>
                            <li><a href="#">José Luis Escalente</a></li>
                            <li><a href="#">Mariangel Hernández</a></li>
                        </ul>
                    </div>

                    <div class="gr-2 gr-12@mobile">
                        <h4>Servicios</h4>
                        <ul>
                            <li><a href="#">Shirley Castillo</a></li>
                            <li><a href="#">María del Mar Pérez</a></li>
                            <li><a href="#">Danubia Madrigal</a></li>
                            <li><a href="#">Hazzel Villegas</a></li>
                        </ul>
                    </div>

                    <div class="gr-2 gr-12@mobile">
                        <h4>Mercadeo</h4>
                        <ul>
                            <li><a href="#">Sergio Salas</a></li>
                            <li><a href="#">Allen Solorzano</a></li>
                        </ul>
                    </div>

                    <div class="gr-2 gr-12@mobile">
                        <h4>Financiero</h4>
                        <ul>
                            <li><a href="#">Arelys Matarrita</a></li>
                        </ul>
                    </div>

                    <div class="gr-2 gr-12@mobile">
                        <h4>Dirección</h4>
                        <ul>
                            <li><a href="#">Carlos Andrés Rojas</a></li>
                            <li><a href="#">Vitaliano Rojas</a></li>
                        </ul>
                    </div>

                    <div class="gr-2 gr-12@mobile footer__people--phones">
                        <h4>Teléfonos</h4>
                        <ul>
                            <li><a href="tel:26613247">2661-3247</a></li>
                            <li><a href="tel:26614056">2661-4056</a></li>
                            <li><a href="tel:26613251">2661-3251</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="gr-2 gr-12@mobile footer__social">
                <div class="footer__social-icons">
                    <a class="vm__circle-icon footer__social-icon footer__social-icon--fb" href="#">
                        <i class="fa fa-fw fa-facebook"></i>
                    </a>
                    <a class="vm__circle-icon footer__social-icon footer__social-icon--tw" href="#">
                        <i class="fa fa-fw fa-twitter"></i>
                    </a>
                    <a class="vm__circle-icon footer__social-icon footer__social-icon--gp" href="#">
                        <i class="fa fa-fw fa-google-plus"></i>
                    </a>
                </div>

                <div>
                    <a class="vm__button vm__button--hollow" href="<?= vp_url('/contacto') ?>">Solicitar información</a>
                </div>
            </div>
        </div>
    </footer>
</div>

<div class="legal-footer-wrapper">
    <footer class="container footer">
        <div class="row">
            <div class="gr-12 footer__copyright">
                &copy; 2016 Derechos Reservados &ndash; Universidad Castro Carazo Puntarenas
            </div>
        </div>
    </footer>
</div>
