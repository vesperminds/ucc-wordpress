<?php defined('ABSPATH') or die;

define('DEBUG_MODE', strpos($_SERVER['SERVER_NAME'], '0.0.0.0') !== false);

add_action('wp_enqueue_scripts', function() {
    $bowerDir = '/../../../bower_components';

    // CSS

    $sheets = [
        [ 'main', '/dist/css/main.css' ],
        [ 'swiper-css', $bowerDir . '/Swiper/dist/css/swiper.min.css' ],
    ];

    foreach ($sheets as $sheet) {
        wp_register_style(
            $sheet[0],
            vpth_path($sheet[1]),
            null,
            vpth_version($sheet[1]),
            'screen'
        );
        wp_enqueue_style($sheet[0]);
    }

    // JS

    $scripts = [
        [ 'jquery', $bowerDir . '/jquery/dist/jquery.min.js' ],
        [ 'angular', $bowerDir . '/angular/angular.min.js' ],
        [ 'swiper', $bowerDir . '/Swiper/dist/js/swiper.jquery.min.js' ],
        [ 'app', '/dist/js/scripts/app.js' ],
        [ 'templates', '/dist/js/templates/templates.js' ],
    ];

    foreach ($scripts as $script) {
        $ext = isset($script[2]) && $script[2];

        wp_deregister_script($script[0]);
        wp_register_script(
            $script[0],
            $ext ? $script[1] : vpth_path($script[1]),
            null,
            $ext ? md5($script[1]) : vpth_version($script[1]),
            true
        );
    }

    foreach($scripts as $script) {
        wp_enqueue_script($script[0]);
    }
});

/* Title */

$_overrideTitle = null;

function th_title_suffix() {
    return ' &mdash; Universidad Castro Carazo';
}

function th_set_page_title($title = 'Universidad Castro Carazo') {
    global $_overrideTitle;

    if (empty($title)) {
        return;
    }

    $_overrideTitle = $title;
}

function th_title() {
    return wp_title(th_title_suffix(), true, 'right');
}

add_filter('wp_title', function($title) {
    global $_overrideTitle;

    if (!empty($_overrideTitle)) {
        return $_overrideTitle . th_title_suffix();
    }

    if (empty($title) && (is_home() || is_front_page())) {
        return 'Universidad Castro Carazo';
    }

    return $title;
});

/* Images */

add_theme_support('post-thumbnails', [
    'post',
    'page',
    'career',
]);

add_action('init', function() {
    add_image_size('vesper-hero-image', 1200, 740);
    add_image_size('vesper-mobile-hero-image', 960, 592);
    add_image_size('vesper-post-thumbnail', 600, 600, true);
    add_image_size('vesper-post-thumbnail-cover', 600, 600);
});

/* JSON */

function tt_json_attr($data = null) {
    return htmlspecialchars(json_encode($data), ENT_QUOTES);
}

function tt_json_output($data) {
    @header('Content-Type: application/json; charset=utf-8');
    echo json_encode($data);
}
