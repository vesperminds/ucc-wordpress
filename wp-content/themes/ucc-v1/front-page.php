<?php get_template_part('parts/head') ?>
<?php get_template_part('sections/front-page', 'header'); ?>

<?php get_template_part('sections/front-page', 'featured'); ?>
<?php get_template_part('sections/front-page', 'admission'); ?>

<?php get_template_part('parts/careers'); ?>
<?php get_template_part('parts/tail') ?>
