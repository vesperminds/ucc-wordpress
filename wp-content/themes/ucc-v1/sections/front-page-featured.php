<?php
    // update_option('ucc_featured_pages', [ 6,8,10,12 ], true);
    $query = vp_inject_pages_from_options('ucc_featured_pages');

    if (!$query->have_posts()) {
        return;
    }
?>
<section class="container featured">
    <div class="row nowrap wrap@mobile">
        <?php while ($query->have_posts()): $query->the_post(); ?>
        <?php
            $bg = get_the_post_thumbnail_url(null, 'vesper-post-thumbnail-cover');
        ?>
        <div class="gr-3 gr-12@mobile gr-table@non-mobile">
            <div class="featured__item">
                <div class="featured__picture"
                    <?= $bg ? "style=\"background-image: url('{$bg}')\"" : '' ?>
                ></div>
                <h3 class="featured__title"><?= the_title() ?></h3>
                <div class="featured__content">
                    <?php the_excerpt() ?>

                    <a class="featured__permalink" href="<?php the_permalink() ?>">- Ver más</a>
                </div>
            </div>
        </div>
        <?php endwhile; ?>
    </div>
</section>
<?php vp_inject_end(); ?>
