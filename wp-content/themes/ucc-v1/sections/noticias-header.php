<div class="header noticias-header">
    <div class="container">
        <div class="row">
            <div class="gr-8 gr-12@mobile">
                <div class="header__heading">
                    Noticias UCC
                </div>

                <div class="header__subheading">
                    Desde 1936, cambiando historias.
                </div>
            </div>
        </div>
    </div>
</div>
