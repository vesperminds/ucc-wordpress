<div class="header contacto-header">
    <div class="container">
        <div class="row">
            <div class="gr-8 gr-12@mobile">
                <div class="header__heading">
                    Contacto UCC
                </div>

                <div class="header__subheading">
                    Desde 1936, cambiando historias.
                </div>
            </div>

            <div class="gr-3 prefix-1 hide@mobile">
                <!-- LAST 3 POSTS -->
            </div>
        </div>
    </div>
</div>
