<?php
    $success = vp_inject_single_page('admision');

    if (!$success) {
        echo "<!-- NO ADMISSION PAGE -->\n";
        return;
    }

    $bg = get_the_post_thumbnail_url(null, 'vesper-post-thumbnail-cover');
?>
<section class="container admission">
    <div class="row admission__title-row">
        <div class="gr-12">
            <h2><?php the_title() ?></h2>
        </div>
    </div>

    <div class="row admission__content-row">
        <div class="gr-3 gr-12@mobile">
            <div class="admission__picture" style="background-image: url('<?= $bg ?>')"></div>
        </div>
        <div class="gr-6 gr-12@mobile">
            <div class="admission__content">
                <?php the_excerpt() ?>

                <a class="admission__view-more" href="<?php the_permalink() ?>">- Ver más</a>
            </div>
        </div>

        <div class="gr-3 gr-12@mobile">
            <h3 class="admission__academic-offer-title">
                Oferta académica<br>
                <?= get_option('ucc_academic_offer', '') ?>
            </h3>

            <ul class="admission__academic-offer-content">
                <li>
                    <a href="<?php the_permalink() ?>#matricula">
                        <i class="fa fa-fw fa-file-text-o"></i>
                        <span>Matrícula</span>
                    </a>
                </li>
                <li>
                    <a href="<?php the_permalink() ?>#admision">
                        <i class="fa fa-fw fa-check-square-o"></i>
                        <span>Admisión</span>
                    </a>
                </li>
                <li>
                    <a href="<?php the_permalink() ?>#becas">
                        <i class="fa fa-fw fa-suitcase"></i>
                        <span>Becas</span>
                    </a>
                </li>
                <li>
                    <a href="<?php the_permalink() ?>#aranceles">
                        <i class="fa fa-fw fa-calculator"></i>
                        <span>Aranceles</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</section>
<?php vp_inject_end(); ?>
