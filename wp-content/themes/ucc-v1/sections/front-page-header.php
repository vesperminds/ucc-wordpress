<div class="header front-page-header">
    <div class="container">
        <div class="row">
            <div class="gr-8 gr-12@mobile">
                <div class="header__heading">
                    Nosotros tenemos fe en tu futuro
                </div>

                <div class="header__subheading">
                    Búscanos y estudia con nosotros hoy.
                </div>
            </div>

            <div class="gr-3 prefix-1 hide@mobile">
                <!-- LAST 3 POSTS -->
            </div>
        </div>
    </div>
</div>
