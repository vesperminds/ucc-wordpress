<?php get_template_part('parts/head') ?>

<?php
    wp_reset_postdata();

    $thumb_id = get_post_thumbnail_id(get_the_ID());
    $thumb_url = '';

    if ($thumb_id) {
        $thumb_url = wp_get_attachment_url($thumb_id);; 
    }
?>
<div class="header" <?= $thumb_url ? "style=\"background-image: url('{$thumb_url}')\"" : '' ?>>
    <div class="container">
        <div class="row">
            <div class="gr-8 gr-12@mobile">
                <div class="header__heading">
                    <?php the_title() ?>
                </div>

                <div class="header__subheading">
                    Desde 1936, cambiando historias.
                </div>
            </div>

            <div class="gr-3 prefix-1 hide@mobile">
                <!-- LAST 3 POSTS -->
            </div>
        </div>
    </div>
</div>

<div class="container page">
    <div class="row">
        <div class="gr-4 gr-12@mobile">
            <div class="page__academic-offer">
                <h3>
                    Oferta académica<br>
                    <?= get_option('ucc_academic_offer', '') ?>
                </h3>
                <ul>
                    <?php get_template_part('partials/academic-offer-li') ?>
                </ul>
            </div>
        </div>
        <div class="gr-8 gr-12@mobile page__content">
            <?php the_content() ?>
        </div>
    </div>
</div>

<?php get_template_part('parts/careers'); ?>
<?php get_template_part('parts/tail') ?>
