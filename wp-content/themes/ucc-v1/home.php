<?php get_template_part('parts/head') ?>
<?php get_template_part('sections/noticias', 'header'); ?>

<?php wp_reset_postdata(); ?>
<div class="container post posts">
    <?php while (have_posts()): the_post(); ?>
    <?php
        $thumb_id = get_post_thumbnail_id(get_the_ID());
        $thumb_url = '';

        if ($thumb_id) {
            $thumb_url = wp_get_attachment_url($thumb_id);; 
        }
    ?>
    <div class="row posts__item">
        <div class="gr-4 gr-12@mobile">
            <a href="<?php the_permalink() ?>" class="post__thumb"
                style="<?= $thumb_url ? "background-image: url('{$thumb_url}')" : '' ?>"
            ></a>
        </div>
        <div class="gr-8 gr-12@mobile post__content">
            <h3 class="post__title">
                <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
            </h3>
            <p class="post__time">
                <i class="fa fa-calendar" aria-hidden="true"></i>
                <?= get_the_date() ?>
            </p>
            <p class="post__excerpt">
                <?php the_excerpt() ?>
            </p>
            <p class="post__actions">
                <a class="vm__button" href="<?php the_permalink() ?>">Ver más</a>
            </p>
        </div>
    </div>
    <?php endwhile; ?>

    <div class="row">
        <div class="gr-12 posts__navigation">
            <?php the_posts_pagination([ 'mid_size' => 3, 'screen_reader_text' => ' ' ]); ?>
        </div>
    </div>
</div>

<?php get_template_part('parts/careers'); ?>
<?php get_template_part('parts/tail') ?>
