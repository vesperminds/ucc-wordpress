<?php get_template_part('parts/head') ?>
<?php get_template_part('sections/admision', 'header'); ?>

<div class="container page">
    <div class="row">
        <div class="gr-4 gr-12@mobile">
            <nav class="page__nav">
                <ul>
                    <li>
                        <a href="#matricula">Matrícula</a>
                    </li>
                    <li>
                        <a href="#admision">Admisión</a>
                    </li>
                    <li>
                        <a href="#becas">Becas</a>
                    </li>
                    <li>
                        <a href="#aranceles">Aranceles</a>
                    </li>
                </ul>
            </nav>

            <div class="page__academic-offer">
                <h3>
                    Oferta académica<br>
                    <?= get_option('ucc_academic_offer', '') ?>
                </h3>
            </div>
        </div>
        <div class="gr-8 gr-12@mobile page__content">
            <a id="matricula"></a>
            <h3>Matrícula</h3>
            <h4>Requisitos de matrícula para estudiantes de nuevo ingreso</h4>
            <ul>
                <li>
                    Completar el formulario para Solicitud de matrícula.
                </li>
                <li>
                    Presentar el original y entregar tres fotocopias del certificado de conclusión de estudios de enseñanza diversificada.
                </li>
                <li>
                    Entregar dos fotografías tamaño pasaporte.
                </li>
                <li>
                    Presentar dos copias de la cédula de identidad por ambos lados o constancia de nacimiento.
                </li>
            </ul>
            <h4>Para estudiantes que deseen cursar la Licenciatura</h4>
            <ul>
                <li>
                    Completar el formulario para solicitud de matrícula.
                </li>
                <li>
                    Presentar el título de conclusión de estudios de enseñanza diversificada y el título de bachillerato Universitario.
                </li>
                <li>
                    1 fotocopia de la cédula de identidad, por ambos lados.
                </li>
                <li>
                    2 fotografías tamaño pasaporte.
                </li>
                <li>
                    Matricular el Seminario Académico de Inducción.
                </li>
            </ul>
            <h4>Requisitos de matrícula para estudiantes avanzados</h4>
            <ul>
                <li>
                    Completar el formulario de matrícula.
                </li>
                <li>
                    Aprobar los cursos que requieren requisitos.
                </li>
            </ul>
            
            <a id="admision"></a>
            <h3>Admisión</h3>
            <p>
                Descargue el siguiente formulario si desea realizar una solicitud de admisión a estudios de pregrado. Llenelo con los datos que se le solicitan y presentelo para su trámite en la universidad.
            </p>
            <p>
                <a href=""><i class="fa fa-2x fa-file-pdf-o"></i></a>
            </p>
            <p>
                Descargue el siguiente formulario si desea realizar una solicitud de admisión para cursos libres, bachillerato por madurez o cursos técnicos. Llenelo con los datos que se le solicitan y presentelo para su trámite en la universidad.
            </p>
            <p>
                <a href=""><i class="fa fa-2x fa-file-pdf-o"></i></a>
            </p>

            <h3 id="becas">Becas</h3>
            <h4>Becas empresariales:</h4>
            <p>
                Se otorgan por medio de los convenios de cooperación estratégica con las empresas y organizaciones que apoyan la formación y capacitación de los colaboradores y sus familiares hasta primer grado de consanguinidad.
            </p>
            <h4>Becas de graduados:</h4>
            <p>
                Benefician a todas aquellas personas egresadas de la institución. Becas laborales: Benefician a personas que trabajan en UCC, al igual que a sus familiares.
            </p>
            <h4>Becas de trabajo:</h4>
            <p>
                Benefician a estudiantes que laboran tiempo parcial en UCC y cursan un programa de estudios en la institución.
            </p>

            <a id="aranceles"></a>
            <h3>Aranceles</h3>
            <h4>Estudiantes nuevo ingreso</h4>
            <ul>
                <li>
                    Bachillerato (2 lecciones)
                </li>
                <li>
                    Licenciatura(2 lecciones)
                </li>
                <li>
                    Materia (3 lecciones)
                </li>
                <li>
                    Materia (4 lecciones)
                </li>
                <li>
                    Materia Inglés de la carrera
                </li>
                <li>
                    Materia Inglés de Informática
                </li>
                <li>
                    Materia Administración General
                </li>
                <li>
                    Materia Inglés de Informática para Técnicos
                </li>
                <li>
                    Inglés Conversacional
                </li>
                <li>
                    Confección de Carné
                </li>
                <li>
                    Convalidación de Materias
                </li>
                <li>
                    Constancias
                </li>
                <li>
                    Certificaciones
                </li>
                <li>
                    Cambios de carrera, grupo o materia en período ordinario
                </li>
                <li>
                    Cambios de carrera, grupo o materia en período extraordinario
                </li>
                <li>
                    Seminarios
                </li>
                <li>
                    Examen por suficiencia Inglés
                </li>
                <li>
                    Multa biblioteca por día
                </li>
                <li>
                    Record de notas
                </li>
                <li>
                    Reposición de título de curso libre
                </li>
                <li>
                    Suficiencias normales
                </li>
                <li>
                    Retiro ordinario
                </li>
                <li>
                    Cursos de Informática aplicados a otras carreras
                </li>
            </ul>
            <h4>Estudiantes avanzados</h4>
            <ul>
                <li>
                    Bachillerato (2 lecciones)
                </li>
                <li>
                    Licenciatura(2 lecciones)
                </li>
                <li>
                    Materia (3 lecciones)
                </li>
                <li>
                    Materia (4 lecciones)
                </li>
                <li>
                    Materia Inglés de la carrera
                </li>
                <li>
                    Materia Inglés de Informática
                </li>
                <li>
                    Materia Inglés de Informática para Técnicos
                </li>
                <li>
                    Inglés Conversacional
                </li>
                <li>
                    Derechos de inscripción TCU
                </li>
                <li>
                    Derechos de graduación
                </li>
                <li>
                    Confección de Carné
                </li>
                <li>
                    Convalidación de Materias
                </li>
                <li>
                    Constancias
                </li>
                <li>
                    Certificaciones
                </li>
                <li>
                    Cambios de carrera, grupo o materia en período ordinario
                </li>
                <li>
                    Cambios de carrera, grupo o materia en período extraordinario
                </li>
                <li>
                    Seminarios
                </li>
                <li>
                    Práctica Profesional Docente
                </li>
                <li>
                    Defensa de Tésis
                </li>
                <li>
                    Investigación Dirigida I y II
                </li>
                <li>
                    Examen por suficiencia Inglés
                </li>
                <li>
                    Multa biblioteca por día
                </li>
                <li>
                    Record de notas
                </li>
                <li>
                    Reposición de título de curso libre
                </li>
                <li>
                    Suficiencias normales
                </li>
                <li>
                    Retiro ordinario
                </li>
            </ul>
            <h4>Descargue la versión en PDF para los aranceles</h4>
            <p>
                <a class="with-icons" href="">
                    <i class="fa fa-2x fa-file-pdf-o"></i>
                    <span>Estudiantes nuevo ingreso</span>
                </a>
            </p>
            <p>
                <a class="with-icons" href="">
                    <i class="fa fa-2x fa-file-pdf-o"></i>
                    <span>Estudiantes avanzados</span>
                </a>
            </p>
            <h4>Formas de pago</h4>
            <p>
                En el momento de la matrícula se cancela el costo de ésta; este rubro no es reembolsable. La colegiatura se puede pagar en cuotas según el número de materias y sin ningún interés; en las siguientes fechas: 15 de mayo, 15 de junio, 14 de julio y 14 de agosto de 2015. Después de las fechas de pago correspondientes, se cobrará un 0.33 % diario de recargo moratorio.
            </p>
            <p>
                A partir del 11 de mayo y hasta que finalice la matrícula, se cobrará un 20 % de recargo en el precio de la matrícula.
            </p>
            <p>
                El 24 de mayo finaliza el período de retiros; congelamientos; cambios de materias, de grupos y de carreras; y presentación de cartas de convenio del II-CO-2015. Las materias congeladas se deben matricular el cuatrimestre posterior inmediato, de lo contrario se pierde el beneficio.
            </p>
        </div>
    </div>
</div>

<?php get_template_part('parts/careers'); ?>
<?php get_template_part('parts/tail') ?>
