<li>
    <a href="<?= vp_url('/admision') ?>#matricula">
        <i class="fa fa-fw fa-file-text-o"></i>
        <span>Matrícula</span>
    </a>
</li>
<li>
    <a href="<?= vp_url('/admision') ?>#admision">
        <i class="fa fa-fw fa-check-square-o"></i>
        <span>Admisión</span>
    </a>
</li>
<li>
    <a href="<?= vp_url('/admision') ?>#becas">
        <i class="fa fa-fw fa-suitcase"></i>
        <span>Becas</span>
    </a>
</li>
<li>
    <a href="<?= vp_url('/admision') ?>#aranceles">
        <i class="fa fa-fw fa-calculator"></i>
        <span>Aranceles</span>
    </a>
</li>
