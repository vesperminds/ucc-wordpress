<?php get_template_part('parts/head') ?>
<?php get_template_part('sections/noticias', 'header'); ?>

<?php
    wp_reset_postdata();

    $thumb_id = get_post_thumbnail_id(get_the_ID());
    $thumb_url = '';

    if ($thumb_id) {
        $thumb_url = wp_get_attachment_url($thumb_id);; 
    }
?>

<div class="container page post">
    <div class="row">
        <div class="gr-4 gr-12@mobile">
            <a href="<?php the_permalink() ?>" class="post__thumb"
                style="<?= $thumb_url ? "background-image: url('{$thumb_url}')" : '' ?>"
            ></a>
        </div>
        <div class="gr-8 gr-12@mobile page__content">
            <h3 class="post__title">
                <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
            </h3>
            <p class="post__time">
                <i class="fa fa-calendar" aria-hidden="true"></i>
                <?= get_the_date() ?>
            </p>
            <?php the_content() ?>
        </div>
    </div>
</div>

<?php get_template_part('parts/careers'); ?>
<?php get_template_part('parts/tail') ?>
