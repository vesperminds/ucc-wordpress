<?php get_template_part('parts/head') ?>
<?php get_template_part('sections/contacto', 'header'); ?>

<div class="container page">
    <div class="row">
        <div class="gr-12 page__content">
            <h3>Contáctenos</h3>
            <p>
                Para cualquier consulta o información sobre nuestros cursos o carreras,
                por favor llene el siguiente formulario. Estaremos respondiendo a la
                brevedad posible.
            </p>

            <contact-us
                endpoint="<?= the_permalink() ?>"
                base-url="<?= vpth_path('/') ?>"
            ></contact-us>

            <div class="row">
                <div class="gr-4 gr-12@mobile">
                    <h4 class="heading">Central telefónica</h4>
                    <ul class="plain-list">
                        <li><a href="tel:+50626613247">2661-3247</a></li>
                        <li><a href="tel:+50626614056">2661-4056</a></li>
                        <li><a href="tel:+50626613251">2661-3251</a></li>
                    </ul>
                </div>
                <div class="gr-4 gr-12@mobile">
                    <h4 class="heading">Fax</h4>
                    <ul class="plain-list">
                        <li><a href="tel:+50626614484">2661-4484</a></li>
                    </ul>
                </div>
                <div class="gr-4 gr-12@mobile">
                    <h4 class="heading">Síguenos en</h4>
                    <p>
                        <a class="vm__circle-icon" href="#">
                            <i class="fa fa-fw fa-facebook"></i>
                        </a>
                        <a class="vm__circle-icon" href="#">
                            <i class="fa fa-fw fa-twitter"></i>
                        </a>
                        <a class="vm__circle-icon" href="#">
                            <i class="fa fa-fw fa-google-plus"></i>
                        </a>
                    </p>
                </div>
                <div class="gr-12">
                    Nuestra dirección es: del Hotel Alamar, 50 metros al este, edificio
                    azul con amarillo esquinero. Puntarenas.
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_template_part('parts/careers'); ?>
<?php get_template_part('parts/tail') ?>
