<?php
    $careerTypes = get_career_types();
?>
<?php get_template_part('parts/head') ?>
<?php get_template_part('sections/carreras', 'header'); ?>

<a id="_top"></a>
<div class="container page">
    <div class="row">
        <div class="gr-4 gr-12@mobile">
            <nav class="page__nav">
                <ul>
                    <?php foreach ($careerTypes as $careerType): ?>
                    <li>
                        <a href="#<?= $careerType['slug'] ?>">
                            <?= $careerType['name'] ?>
                        </a>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </nav>

            <div class="page__academic-offer">
                <h3>
                    Oferta académica<br>
                    <?= get_option('ucc_academic_offer', '') ?>
                </h3>
                <ul>
                    <?php get_template_part('partials/academic-offer-li') ?>
                </ul>
            </div>
        </div>
        <div class="gr-8 gr-12@mobile page__content">
            <?php foreach ($careerTypes as $careerType): ?>
            <?php
                $careers = get_all_careers([
                    'orderby' => 'title',
                ], [
                    'term' => $careerType['term_id']
                ]);
            ?>
            <a id="<?= $careerType['slug'] ?>"></a>
            <h3><?= $careerType['name'] ?></h3>
            <div class="row">
                <?php foreach ($careers as $career): ?>
                <div class="gr-4 gr-6@mobile page__grid">
                    <a
                        class="page__grid-cell"
                        href="<?= $career['permalink'] ?>"
                        style="background-image: url('<?= $career['thumb'] ?>')"
                    >
                        <overlay></overlay>
                        <span><?= $career['title'] ?></span>
                    </a>
                </div>
                <?php endforeach; ?>
            </div>
            <p class="page__go-top">
                <a href="#_top">
                    <i class="fa fa-2x fa-arrow-circle-up"></i>
                    <span>Ir arriba</span>
                </a>
            </p>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<?php get_template_part('parts/tail') ?>
