<?php get_template_part('parts/head') ?>
<?php get_template_part('sections/nosotros', 'header'); ?>

<div class="container page">
    <div class="row">
        <div class="gr-4 gr-12@mobile">
            <nav class="page__nav">
                <ul>
                    <li>
                        <a href="#trayectoria">Trayectoria</a>
                    </li>
                    <li>
                        <a href="#mision">Misión</a>
                    </li>
                    <li>
                        <a href="#vision">Visión</a>
                    </li>
                    <li>
                        <a href="#valores">Valores</a>
                    </li>
                    <li>
                        <a href="#perfil-de-graduado">Perfil de Graduado</a>
                    </li>
                </ul>
            </nav>

            <div class="page__academic-offer">
                <h3>
                    Oferta académica<br>
                    <?= get_option('ucc_academic_offer', '') ?>
                </h3>
                <ul>
                    <?php get_template_part('partials/academic-offer-li') ?>
                </ul>
            </div>
        </div>
        <div class="gr-8 gr-12@mobile page__content">
            <a id="trayectoria"></a>
            <h3>Trayectoria</h3>
            <p>
                En 1936, el profesor Miguel Ángel Castro Carazo fundó la escuela que lleva sus apellidos, la cual estaba destinada a formar recursos humanos calificados para las necesidades del desarrollo de la época: Teneduría de Libros, Contabilidad, Mecanografía, Taquigrafía y Redacción Comercial.
            </p>
            <p>
                Durante 1937, siendo presidente de la República León Cortés Castro, el Congreso Constitucional le concedió una subvención de 300 colones mensuales para que becara a alumnos que estudiaban por correspondencia y en forma presencial.
            </p>
            <p>
                En 1944, el presidente de la República, Teodoro Picado Michalski, mediante el decreto 124 del 13 de octubre, dio la autorización para que la Escuela expidiera títulos de Mecanografía, Taquigrafía, Redactor Comercial, Tenedor de Libros, Contador Mercantil, Auditor y Calculador Mercantil.
            </p>
            <p>
                Durante 1947, la institución se consolidó al recibir el reconocimiento de la National Association of Accredited Business School, de Estados Unidos. Con tal certificación, quedó habilitada para atender las necesidades educativas de los veteranos que habían quedado sin trabajo al final de la Segunda Guerra Mundial. Su trayectoria continuó aquilatando prestigio y, en 1992, el Consejo Superior de Educación le otorgó la categoría de Para universitaria Castro Carazo, lo que le permitió expedir títulos de diplomado. Esta institución alcanza la cúspide del desarrollo organizacional cuando es declarada Universidad Metropolitana Castro Carazo, por el Consejo Nacional de Enseñanza Superior Universitaria Privada, en acuerdo Nº305-96, del 29 de julio de 1996.
            </p>
            <p class="page__section-image">
                <img src="<?= vpth_path('/img/trayectoria-img.png') ?>">
            </p>

            <a id="mision"></a>
            <h3>Misión</h3>
            <p>
                Por su prestigio y trayectoria, la Universidad Castro Carazo (UCC) está llamada a garantizar, a todas las personas, sin distinción de origen étnico, género, edad, capacidades diferentes, condición social, condiciones de salud, religión, opiniones, preferencias o estado civil, el acceso a ambientes de calidad para la enseñanza y el aprendizaje.
            </p>
            <p>
                A través de una educación superior pertinente y eficaz, UCC pretende contribuir con el bienestar individual y colectivo,  desarrollando en sus alumnos la capacidad de lograr sus sueños, metas profesionales y potencial intelectual pleno. Asimismo, los faculta para asumir su responsabilidad solidaria, como ciudadanos emprendedores, productivos, creativos y éticos, en un mundo cambiante y multicultural.
            </p>
            <p>
                Para ello, UCC ofrece programas formales y no-formales de educación general, ocupacional y de conservación ambiental. También provee servicios de desarrollo y apoyo estudiantil, para asegurar el éxito de sus alumnos en su plan de vida.
            </p>
            <p class="page__section-image">
                <img src="<?= vpth_path('/img/mision-img.png') ?>">
            </p>

            <a id="valores"></a>
            <h3>Valores</h3>
            <p>
                Subyacen a la misión y visión institucionales los siguientes valores, inspirados por su fundador, Miguel Ángel Castro Carazo, que sirven como base de la cultura organizacional.
            </p>
            <h4>Honestidad</h4>
            <p>
                “No se triunfa con el dinero que quema las manos y la conciencia. Quienes han amasado su fortuna con lágrimas ajenas, estrujando al necesitado, explotando al pueblo, se han distanciado de su propia conciencia y han cambiado su felicidad por un puñado de monedas. Solamente las satisfacciones del espíritu son las verdaderas y las hondas. Y estas se adquieren tratando a los demás como quisiéramos ser tratados por ellos”.
            </p>
            <h4>Sinceridad</h4>
            <p>
                “Debemos tener la firme convicción de que el mundo estará de nuestra parte, siempre que seamos sinceros con nosotros mismos y con lo mejor que llevamos de nuestro ser”.
            </p>
            <h4>Solidaridad</h4>
            <p>
                “El mundo está compuesto de dos clases de personas: las que viven para sí mismas, buscando riquezas y bienes materiales- que son precisamente los causantes de las guerras y de la pobreza- y las que viven dando de sí, pensando en el prójimo y procurando servirle sin buscar recompensa: estas son las que traen paz y prosperidad. Usemos de nuestra libertad para colmarla de paz y de prosperidad. Pensemos un poco más en nuestros semejantes y un poco menos en nosotros mismos”.
            </p>
            <h4>Sensibilidad humana</h4>
            <p>
                “El hombre no puede ni debe vivir sumido en la materialidad, porque a eso no podría llamarse vida. La vida es lo real, lo verdadero, lo eterno: lo que atañe al espíritu. Lo demás es transitorio y, por consiguiente, deleznable. Solo vive quien lleva un alma dentro de su cuerpo y quien tiene en ella sensibilidad”.
            </p>
            <h4>Justicia</h4>
            <p>
                “Alcanzar éxito es ejecutar nuestro trabajo lo mejor que podamos, ser justos con el prójimo”.
            </p>
            <h4>Ética</h4>
            <p>
                “Alejémonos del camino fácil, que siguen algunos en sus negocios y en sus relaciones con el público, y que consiste en pensar que la ética no es más que un en trabamiento que obstaculiza los esfuerzos y la energía desarrollada por ellos, para conseguir ganancias fáciles, por medios no del todo lícitos. Hay hombres que carecen de principios de ética y que no son deseables en los negocios”.
            </p>
            <h4>Responsabilidad</h4>
            <p>
                “Es preferible no prometer nada, que dejar de cumplir lo que hemos ofrecido”.
            </p>
            <h4>Determinación</h4>
            <p>
                “Nada que valga la pena llega al hombre que se sienta a soñar, si no pone la acción detrás, para convertir el sueño en realidad. Un espíritu indomable y la determinación de triunfar pueden vencer las más tremendas circunstancias”.
            </p>
            <p class="page__section-image">
                <img src="<?= vpth_path('/img/valores-img.png') ?>">
            </p>

            <a id="perfil-de-graduado"></a>
            <h3>Perfil de Graduado</h3>
            <p>
                El graduado de UCC estará en capacidad de:
            </p>
            <ul>
                <li>
                    Aplicar los conocimientos, métodos, herramientas y formas de pensar de su disciplina en la resolución de problemas complejos.
                </li>
                <li>
                    Mostrar habilidades interpersonales, como el liderazgo, la negociación, el trabajo en equipo, la empatía, el respeto, la asertividad y el interés por conocer las necesidades y motivaciones de otros.
                </li>
                <li>
                    Emplear su creatividad para diseñar nuevos productos o servicios que agreguen valor a la sociedad.
                </li>
                <li>
                    Trabajar y compartir con otros en ambientes interdisciplinarios y multiculturales.
                </li>
                <li>
                    Evaluar sus propias competencias y conducta, estableciéndose metas específicas y realistas, analizando su progreso e implementando medidas correctivas, con el fin de mejorar continuamente.
                </li>
                <li>
                    Gestionar sus propios emprendimientos, con sostenibilidad ambiental.
                </li>
                <li>
                    Vivir de forma disciplinada y tener hábitos productivos.
                </li>
                <li>
                    Saber escuchar y expresarse, persuasivamente, de forma oral y en público.
                </li>
                <li>
                    Saber leer y expresarse por escrito, empleando la lógica y la buena redacción.
                </li>
                <li>
                    Ejercer su responsabilidad cívica con honestidad e inteligencia.
                </li>
                <li>
                    Pensar de forma crítica, analítica, creativa y sintética.
                </li>
                <li>
                    Tomar decisiones efectivamente, identificando metas, escogiendo alternativas, obteniendo y valorando la información requerida y determinando cómo implementar la mejor opción.
                </li>
                <li>
                    Emplear medios tecnológicos para acceder a la información y procesarla.
                </li>
                <li>
                    Trabajar con energía, optimismo y calidad.
                </li>
                <li>
                    Hablar más de un idioma.
                </li>
                <li>
                    Actuar con ética.
                </li>
            </ul>
        </div>
    </div>
</div>

<?php get_template_part('parts/careers'); ?>
<?php get_template_part('parts/tail') ?>
